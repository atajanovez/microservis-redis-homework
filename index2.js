const express = require("express");
const {createClient} = require("redis")

const redisClient = createClient()
redisClient.on('error', err => console.log('Redis Client Error', err));

async function startRedisServer(){
    await redisClient.connect();
}

startRedisServer()

const app = express();
const port = 8003;

app.use(express.json());
// app.use

app.delete('/del/:key', async (req, res)=>{
    const {key} = req.params

redisClient.del(key)
.then(response=>{
    console.log("response of redis:",response)
    res.send("ok")
})
.catch(err=>{
    res.send("some problem occurred.." + err)
})
})


app.listen(port, () => console.log("Server running on port ", port));
