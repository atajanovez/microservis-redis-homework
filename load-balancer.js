const express = require("express");
const {createClient} = require("redis")
const { default: axios } = require("axios");


const redisClient = createClient()
redisClient.on('error', err => console.log('Redis Client Error', err));

async function startRedisServer(){
    await redisClient.connect();
}

startRedisServer()

const app = express()
const port = 8000

app.use(express.json())
// app.use

app.get("/:server", async (req, res) => {
    const { server } = req.params;
  
    const isValidServer = [1, 2, 3].includes(Number(server));
  
    if (!isValidServer) {
      res.send("not a valid request");
      return;
    }
  
    axios
      .get(`http://localhost:800${server}`)
      .then((response) => {
        console.log("status",response.status)
        console.log("data", response.data);
        res.json(response.data);
      })
      .catch((err) => {
        console.log(err)
        res.send("Error in server:", server);
      });
  });
app.listen(port, ()=>console.log('Server running on port ', port))

