const express = require("express");
const {createClient} = require("redis")

const redisClient = createClient()
redisClient.on('error', err => console.log('Redis Client Error', err));

async function startRedisServer(){
    await redisClient.connect();
}

startRedisServer()

const app = express();
const port = 8001;

app.use(express.json());
// app.use

app.get('/get/:key', async (req, res)=>{
  const {key} = req.params

redisClient.get(key)
.then(response=>{
  console.log("response of redis:",response)
  if(response===null){
      res.send("key doesn't exist")
      return;
  }
  res.send(response)
})
.catch(err=>{
  res.send("some problem occurred.." + err)
})
})

app.listen(port, () => console.log("Server running on port ", port));
