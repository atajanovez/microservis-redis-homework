const express = require("express");
const {createClient} = require("redis")

const redisClient = createClient()
redisClient.on('error', err => console.log('Redis Client Error', err));

async function startRedisServer(){
    await redisClient.connect();
}

startRedisServer()

const app = express();
const port = 8002;

app.use(express.json());
// app.use

app.post('/set', async (req,res)=>{
    const {key,value}=req.body

    redisClient.set(key,value)
    .then(response=>{
        console.log("response of redis:",response)
        res.send(response)
    })
    .catch(err=>{
        res.send("some problem occurred.." + err)
    })
})


app.listen(port, () => console.log("Server running on port ", port));
